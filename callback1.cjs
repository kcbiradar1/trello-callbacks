const board_detail = require('./data/boards.json')

function get_board_information(callback, board_id) {
  setTimeout(() => {
    const result = board_detail.filter(
      (each_board) => each_board.id === board_id
    );
    callback(null, result);
  }, 2000);
}

module.exports = get_board_information;
