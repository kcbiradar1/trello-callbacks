const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')

const board_details = require('./data/boards.json');
const list_details = require('./data/lists_1.json');

function getInformation(name) {
    const board_id = board_details.filter((each_board) => {
        if(each_board.name === name) {
            return each_board;
        }
    })[0].id;

    callback1((err , data) => {
        if(err) {
            console.log(err);
        } else {
            console.log(data);
        }
    } , board_id);

    callback2((err,data) => {
        if(err) {
            console.log(err);
        } else {
            console.log(data);
        }
    } , board_id);

    let ids = []
    Object.keys(list_details).map((each) => {
        list_details[each].map(inside_each => {
            ids.push(inside_each.id);
        })
    });

    for(let index in ids) {
        callback3((err,data) => {
            if(err) {
                console.log(err);
            } else {
                if(data !== undefined) {
                    console.log(data);
                }
            }
        } , ids[index]);
    }

}

module.exports = getInformation;