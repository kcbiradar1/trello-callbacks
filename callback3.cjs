const card_details = require("./data/card_2.json");

function get_card_details(callback, list_id) {
  setTimeout(() => {
    const result =
      card_details[
        Object.keys(card_details).filter((each_card) => {
          return each_card === list_id;
        })
      ];
    callback(null, result);
  }, 2000);
}

module.exports = get_card_details;
