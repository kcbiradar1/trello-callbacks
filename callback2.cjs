const lists = require("./data/lists_1.json");

function allListsByBoardID(callback, board_id) {
  setTimeout(() => {
    const result = lists[Object.keys(lists).filter(each_id =>  each_id === board_id)];
    callback(null, result);
  }, 2000);
}

module.exports = allListsByBoardID;
